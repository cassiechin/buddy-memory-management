/*
 * Cassie Chin, Jeremy Tinker, Hollie King
 * CSCE 4600, Project 2
 * Buddy Memory Mangement System
 * 
 * Process Memory Range = 1 KB - 100 KB
 * Max Space Allowed = 10 MB = 10000 KB > 2^13 = 8192 KB
 * 
 */
#include "buddyMemory.h"
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <stdio.h>

#define MAX_ORDER 			(13)
#define MIN_BLOCK_SIZE 		(1)
#define MAX_BLOCK_SIZE 		(8192)
#define MAX_NUM_MIN_BLOCK 	(MAX_BLOCK_SIZE / MIN_BLOCK_SIZE)

enum blockStatus { 
	START_USED_BLOCK,
	PART_USED_BLOCK, 
	START_FREE_BLOCK, 
	PART_FREE_BLOCK 
};

/**
 * Initialize a block.
 * This block is the smallest unit block possible (order 0) block
 * @param start Blocks are identified by their start number.
 */
Block::Block(int start) {
	startBlock = start;
    endBlock = start + MIN_BLOCK_SIZE;
	status = START_FREE_BLOCK;
	order = 0;
	mem = 0;
    next = NULL;
}

/**
 * Initalize an empty block list.
 */
BlockList::BlockList() {
	head = NULL;
	tail = NULL;
	size = 0;
}

/**
 * Add a block to the end of the block list
 * @param b The block to append.
 */
bool BlockList::add_block(Block *b) {
	if (!b) return false;
	
	b->next = NULL; // we're putting the block at the end of the list
	if (size++ == 0) head = tail = b;
	else  tail = tail->next = b;
	
	return true;
} 

/**
 * Removes the first block from the block list if it exists
 * @return The first block.
 */
Block* BlockList::remove_first_block() {
	if (size == 0) return NULL;
	
	Block *ret = head;
	if (size-- == 1) head = tail = NULL;
	else head = head->next;

	return ret;
}

/**
 * Removes a block if it exists
 * @param b The block to search for and remove.
 */
void BlockList::remove_block(Block *b) {
	if (size == 0) return;

	// Remove head
	if (head->startBlock == b->startBlock) {
		head = head->next;
		size--;
		return;
	}
	// Remove tail
	else {
		for (Block* i=head; i->next; i=i->next) {
			// check to see if the NEXT one is the block
			if (i->next->startBlock == b->startBlock) {
				i->next = i->next->next;
				size--;
				return;
			}
		}
	}
}

/**
 * Figure out which order block a size of memory needs.
 * @param size The memory size that needs a block to fit into.
 * @return The order of the block.
 */
int get_lowest_order (int size) {
	if (size < 0 || size > MAX_BLOCK_SIZE) return ERROR;
	if (size <= MIN_BLOCK_SIZE) return 0;
	
	int mult2 = 2;
	for (int i=1; i<MAX_ORDER; i++) {
		if (size <= (MIN_BLOCK_SIZE * mult2)) return i;
		mult2 *= 2;
	}
		
	return MAX_ORDER;
}

/**
 * Sets a block's (can contain multiple "unit" blocks) status USED
 * @param s The starting block number
 * @param e The ending block number
 * @param o The order of the set of blocks
 */
void BuddyMemory::setBlockUsed (int s, int e, int o) {
	blockVector[s]->status = START_USED_BLOCK;
	blockVector[s]->endBlock = e;
	blockVector[s]->order = o;
	for (int i=s+1; i<e; i++) {
		blockVector[i]->status = PART_USED_BLOCK;
		blockVector[i]->order = o;
	}
}

/**
 * Sets a block's (can conatin multiple "unit" blocks) status to FREE
 * @param s The starting block number
 * @param e The ending block number
 * @param o The order of the set of blocks
 */
void BuddyMemory::setBlockFree (int s, int e, int o) {
	blockVector[s]->status = START_FREE_BLOCK;
	blockVector[s]->endBlock = e;
	blockVector[s]->order = o;
	for (int i=s+1; i<e; i++) {
		blockVector[i]->status = PART_FREE_BLOCK;
		blockVector[i]->order = o;
	}	
}

/**
 * Remove block from its order vector.
 * @param b The block number of the block to remove
 */
void BuddyMemory::removeBlockFromOrder (int b) {
	orderVector[blockVector[b]->order]->remove_block(blockVector[b]);
}

/**
 * Splits a block of a certain order number into 2 blocks.
 * @param order The order of the block to split.
 * @return True if split was successful, false if unsuccessful
 */
bool BuddyMemory::split_first_block (int order) {
	// There's nothing in the block list to split
	if (orderVector[order]->size == 0) return false;
	
	// Remove the first block from the list
	Block *b = orderVector[order]->remove_first_block();
	int start = b->startBlock;
	int end = b->endBlock;
	int mid = (start + end) / 2;
	
	// Make new boundaries and set the status of these blocks to free
	setBlockFree(start, mid, order-1);
	setBlockFree(mid, end, order-1);
	
	// Add the block to it's order list
    orderVector[b->order]->add_block(blockVector[start]);    
    orderVector[b->order]->add_block(blockVector[mid]);
    
    // splitting one block into two => so there is one more block free
    blocksFree++; 
    return true;
}

/**
 * Get the block number of a memory address.
 * @param a The address
 * @return The block number
 */
int BuddyMemory::addressToBlock (void *a) {
	if ( a < (void*) &memory[0] ) return ERROR;
	if ( a > (void*) &memory[MAX_NUM_MIN_BLOCK-1] ) return ERROR;
	
	// Binary Search for address
	int x = 0;
	int y = MAX_NUM_MIN_BLOCK;	
	while (x <= y) {
		int mid = (x + y) / 2;
		if ( (void*) &memory[mid] == a ) return mid;
		if ( (void*) &memory[mid] >  a ) y = mid;		
		if ( (void*) &memory[mid] >  a ) x = mid + 1;
	}
	
	return ERROR;
}

/**
 * Get the memory address of a block number
 * @param b The block number
 * @return The memory address
 */
void *BuddyMemory::blockNumToAddress (int b) {
	return (void*) &memory[b];
}

/**
 * Places and takes blocks off the orderVector.
 */
BuddyMemory::BuddyMemory() {
	memory = (char *) calloc (MAX_BLOCK_SIZE, sizeof(char));
	for (int i=0; i<MAX_NUM_MIN_BLOCK; i++) blockVector.push_back(new Block(i));
	for (int i=0; i<=MAX_ORDER; i++) orderVector.push_back(new BlockList());
		
	// Put the first block in the order[MAX_ORDER] vector
	setBlockFree(0, MAX_NUM_MIN_BLOCK, MAX_ORDER);
	orderVector[MAX_ORDER]->add_block(blockVector[0]);
	
	memAllocated = 0;
	memFree = MAX_BLOCK_SIZE;
	blocksAllocated = 0;
	blocksFree = 1;
}

/**
 * Returns the memory address of a block 
 * @param size The amount of memory needed in KB
 * @return An address
 */
void* BuddyMemory::my_malloc(int size) {
	int order = get_lowest_order(size);
	
	while (true) {
		// Find a block of the lowest possible order
		Block *b = orderVector[order]->remove_first_block();
		
		// If the block was found...
		if (b) {
			// Remove the block from the order vector
			setBlockUsed(b->startBlock, b->endBlock, order);	
			removeBlockFromOrder(b->startBlock);
			
			// Update statistics
			b->mem = size;		
			int tSize = (b->endBlock - b->startBlock) * MIN_BLOCK_SIZE;
			memFree-=tSize;
			memAllocated+=tSize;
			memFragmentedFree+=(tSize - size);
			memFragmentedAllocated+=size;			
			blocksAllocated++;
			blocksFree--;

			return blockNumToAddress(b->startBlock);
		}
		// BNo block of that order, split the next biggest block
		else {
			bool split = false;
			for (int i=order+1; i<=MAX_BLOCK_SIZE; i++) {
				// Split the next biggest block
				if (orderVector[i]->size >= 1) {
					split = true;
					split_first_block(i);	
					break;
				}
			}
			// If there are no blocks to split, then malloc fails
			if (split == false) return NULL; 
		}
	}
	
    return NULL;  
}

/**
 * Free a block of memory
 * @param blockAdress The address of the block to free
 * @return True if the address was successfully freed, False if not
 */
bool BuddyMemory::my_free(void* blockAddress) {
	// Only free a valid address
	int block = addressToBlock(blockAddress);
	if (block == ERROR) return false;	
	if (blockVector[block]->status != START_USED_BLOCK) return false;
	
	// Update statistics
	int totalBlockSize = (blockVector[block]->endBlock - blockVector[block]->startBlock) * MIN_BLOCK_SIZE;
	memFree+=totalBlockSize;
	memAllocated-=totalBlockSize;
	memFragmentedFree-=(totalBlockSize - blockVector[block]->mem);
	memFragmentedAllocated-=blockVector[block]->mem;
	blocksAllocated--;
	blocksFree++;
	
	// You can only put two blocks together if they are the same size
	for (int i=blockVector[block]->endBlock; i<MAX_NUM_MIN_BLOCK; i++) {
		// Stop merging if we reach a used block, or a block that is not the same size
		if (blockVector[i]->status == START_USED_BLOCK || blockVector[i]->status == PART_USED_BLOCK) break;
		if (blockVector[i]->status == START_FREE_BLOCK && blockVector[i]->order != blockVector[block]->order) break;

		// Merge the blocks if there is a free block that is the same size/order
		if (blockVector[i]->status == START_FREE_BLOCK && blockVector[i]->order == blockVector[block]->order) {
			blockVector[block]->endBlock = blockVector[i]->endBlock; // set the end of the current block to the end of the block that was just merged in
			blockVector[block]->order++; // Increasing the size of the block increases its order
			removeBlockFromOrder(i); // remove the merged block from the order vector
			blocksFree--;
			i = blockVector[i]->endBlock - 1; // update the block counter to after the block that was just freed
		}
	}
	
	// Add the merged blocks to the order vector
	int order = blockVector[block]->order;
	setBlockFree(block, blockVector[block]->endBlock, order);
	orderVector[order]->add_block(blockVector[block]);
		
	return true;
}

/**
 * Print functions for debugging
 */
void Block::print() {
	cout << startBlock << "\t" << endBlock << "\t" << order << "\t";
	switch (status) {
		case START_USED_BLOCK: cout << "ALLOCATED"; break;
		case PART_USED_BLOCK: cout << "|"; break;
		case START_FREE_BLOCK: cout << "FREE"; break;
		case PART_FREE_BLOCK: cout << "|"; break;
	}
	cout << endl;
}
void BlockList::print() { 
	for (Block *b = head; b; b=b->next) b->print(); 
}
void BuddyMemory::print() {
	cout << "memFree: " << memFree << ", ";
	cout << "memAllocated: " << memAllocated << ", ";
	cout << "memFragmentedFree: " << memFragmentedFree << ", ";
	cout << "memFragmentedAllocated: " << memFragmentedAllocated << ", ";
	cout << "blocksFree: " << blocksFree << ", ";
	cout << "blocksAllocated: " << blocksAllocated << endl;
}
void BuddyMemory::print_orderVector() {
	for (int i=MAX_ORDER; i >= 0; i--) {
		cout << "ORDER " << i << endl;
		orderVector[i]->print();
		cout << endl;
	}
}
void BuddyMemory::print_blockVector() { 
	cout << "Start\tEnd\tOrder" << endl;
	for (int i=0; i < MAX_NUM_MIN_BLOCK; i++) blockVector[i]->print(); 
}
