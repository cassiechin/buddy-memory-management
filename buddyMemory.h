/*
 * Cassie Chin, Jeremy Tinker, Hollie King
 * CSCE 4600, Project 2
 * Buddy Memory Mangement System
 * 
 * Usage:
 * int x = my_malloc(int size)
 * if (x != ERROR) free(x);
 */
#ifndef BUDDY_MEMORY_H
#define BUDDY_MEMORY_H

#include <random>
#define ERROR -1
using namespace std;

class Block {
	public:
		int startBlock; // This is never changed after it is initialized
		int endBlock;   // Only needed on blocks where the status is START_BLOCK
		int order;      // The lowest order block is order 0
		int status;     // Says whether this block is allocated/free, and start/part of block
		int mem;	    // Set this when the block is allocated
		Block *next;    // Used when the block is on a BlockList
		
		Block(int); // Take in the startBlock number, automatically set order to 0
		void print();
};

class BlockList {
	public:
		Block *head;
		Block *tail;
		int size;  // The number of blocks in the list
		
		BlockList();
		bool add_block(Block *);     // Add block to the end of the list
		Block *remove_first_block(); // Remove and return the first block
		void remove_block(Block *);  // Remove a block
		void print();
};

class BuddyMemory {
	public:
		char *memory;					 // The memory to return
		vector<Block *> blockVector;	 // All order 0 blocks
		vector<BlockList *> orderVector; // Store blocks that can be allocated
		int blocksAllocated;			 // The number of blocks being used
		int blocksFree;					 // The number of blocks in the orderVector
		int memAllocated;				 // The amount of unusable space (total space of blocks being used, including holes)
		int memFree;					 // The amount of usable space (total space of blocks on the orderVector)
		int memFragmentedAllocated; 	 // The total memory allocated (sum of the memory requests)
		int memFragmentedFree; 			 // The total "hole" space (sum of the holes of the memory requests)
		
		bool split_first_block(int);	  // The block order to split
		void setBlockUsed(int, int, int); // Set status to USED
		void setBlockFree(int, int, int); // Set status to FREE
		void removeBlockFromOrder(int);	  // Remove a input block from an order list
		int addressToBlock (void *a);	  // Get the address of a block
		void *blockNumToAddress (int a);  // Get the block number of an address
	
		BuddyMemory(); 
		void* my_malloc(int size); // takes in size in KB, change to char* later, returns ERROR if no block was found
		bool my_free(void *); 	   // change to char* later
		void print_orderVector();  // print out each order list
		void print_blockVector();  // print out the states of all blocks in the blockVector
		void print();			   // print out BuddyMemory statistics
};
#endif
