/*
 * Cassie Chin, Jeremy Tinker, Hollie King
 * CSCE 4600, Project 2
 * Buddy Memory Mangement System
 * 
 * Usage:
 * int x = my_malloc(int size)
 * if (x != ERROR) free(x);
 */
#include <iostream>
#include "buddyMemory.h"
 
 void test1(BuddyMemory *bm) {
	bm->print();
	char* d1 = (char *) bm->my_malloc(4000); bm->print();	 
 }

 void test2(BuddyMemory *bm) {
	bm->print();
	char* a = (char*) bm->my_malloc(1000); bm->print();
	char* b = (char*) bm->my_malloc(2000); bm->print();
	char* c = (char*) bm->my_malloc(3000); bm->print();
	bm->my_free (b); bm->print();
	bm->my_free (a); bm->print();
	bm->print_orderVector();
 }
 
 void test3(BuddyMemory *bm) {
	 bm->print();
	 char* a01 = (char*) bm->my_malloc(1);
	 char* a02 = (char*) bm->my_malloc(2);
	 char* a03 = (char*) bm->my_malloc(3);
	 char* a04 = (char*) bm->my_malloc(4);
	 char* a05 = (char*) bm->my_malloc(5);
	 char* a06 = (char*) bm->my_malloc(6);
	 char* a07 = (char*) bm->my_malloc(7);
	 char* a08 = (char*) bm->my_malloc(8);
	 char* a09 = (char*) bm->my_malloc(9);
	 bm->print();
	 bm->print_orderVector();
 }

int main () {
	
	#if 1
	int input;
	cout << "Enter test # (1-3): ";
	cin >> input;
	switch(input) {
		case 1: test1(new BuddyMemory()); break;
		case 2:	test2(new BuddyMemory()); break;
		case 3:	test3(new BuddyMemory()); break;
	}
	#endif
	
	return 0;
}
